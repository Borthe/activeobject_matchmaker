package activation_enqueue;

import java.util.ArrayList;
import java.util.List;

public class ActivationList<T> {

	private List<T> queue;

	public ActivationList(int queueLen){
		this.queue = new ArrayList<T>(queueLen);
	}
	
	public void enqueue(T request){
		queue.add((T) request);
	}
	
	//TODO: dequeue
	public T dequeue(){
		T element = queue.get(0);
		queue.remove(0);
		return element;
	}
	
	public int size() {
		return queue.size();
	}
	
}
