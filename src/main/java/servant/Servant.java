package servant;

import java.util.function.Function;

public class Servant<T, V> {
	
	private Function<T, V> function;
	private T parameters;
	
	public V run() {
		V response = function.apply(parameters);
		return response;
	}
	
	public void setFunc(Function<T, V> func) {
		this.function = func;
	}

	public void setParams(T params) {
		this.parameters = params;
	}

}
