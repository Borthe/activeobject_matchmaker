package dispatcher;

import operation.Method_Request;

public interface Dispatcher {
	
	boolean dispatch();
	
	void insert(Method_Request method);
	
}
