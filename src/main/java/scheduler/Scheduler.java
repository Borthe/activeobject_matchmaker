package scheduler;

import activation_enqueue.ActivationList;
import dispatcher.Dispatcher;
import operation.Method_Request;

public class Scheduler implements Dispatcher{

	private ActivationList<Object> queue;
	private Method_Request method_req;
	
	public Scheduler(int queueLen) {
		this.queue = new ActivationList<>(queueLen);
		//TODO: ver si solo hay 1 method request
		this.method_req = new Method_Request(null);
	}
	
	//TODO: dispatch
	@Override
	public boolean dispatch() {
		if (method_req.can_run())
			queue.dequeue();
			return false;
	}
	
	@Override
	public void insert(Method_Request method) {
		queue.enqueue(method);
	}

	public int getEnqueued() {
		return queue.size();
	}
	
}
