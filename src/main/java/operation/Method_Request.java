package operation;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import servant.Servant;

public class Method_Request {

	private boolean running;
	private Servant<?, ?> servant;
	private Future<?> future;
	
	public Method_Request(Future<?> future) {
		this.running = false;
		this.future = future;
	}
	
	public boolean can_run() {
		if (running)
			return false;
		else
			return true;
	}
	
	public void call() {
		writeResult(servant.run());
	}
	
	//TODO: ver donde va el servant
//	private void createServant() {
//		
//	}
	
	private void writeResult(Object object) {
		try {
			object = future.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
